# Py-Parser-un-SVG

Un programme Python pour parser un fichier SVG.
Des infos sur les fichiers SVG et autres fichiers de type XML.
Niveau vrai-débutant à faux-débutant :).

> Merci à Guillaume de https://oiusx.org

# Les fichiers SVG et de type XML

## Pour bien démarrer avec les SVG avec la doc Mozilla :)
- Doc Mozilla en français sur le SVG : https://developer.mozilla.org/fr/docs/web/SVG/Tutorial/Getting_Started

- Avec un éditeur de texte, copier-coller le code ci-dessous un fichier texte avec l'extension .svg. Par exemple : `dessin.svg`

```
<svg version="1.1"
     baseProfile="full"
     xmlns="http://www.w3.org/2000/svg">

  <rect width="100%" height="100%" fill="red"/>

  <circle cx="150" cy="100" r="80" fill="green"/>

  <text x="150" y="125" font-size="60" text-anchor="middle" fill="white">SVG</text>

</svg>

```
- Avec FireFox (ou autre), cliquer sur le fichier ainsi créé, et vous obtiendrez dans votre navigateur :

![Rond contenant le texte SVG](https://media.prod.mdn.mozit.cloud/attachments/2012/07/09/3221/7af6e1cc18f7b247b8a32d1fd9ee926a/svgdemo1.png "Image SVG contruite à partir d'un texte")

- Ou cliquer [**ici**](http://developer.mozilla.org/@api/deki/files/4571/=svgdemo1.xml "Lien vers l'exemple de Mozilla.") pour obtenir l'exemple de la documentation Mozilla :).

## Toute la documentation Mozilla sur le format SVG

- Lien : https://developer.mozilla.org/fr/docs/Web/SVG

- Accès à, entre autres :
  - Liste des **éléments** (`rect` pour le rectangle) : https://developer.mozilla.org/fr/docs/Web/SVG/Element
  - Liste des **attributs** qui vont "modifier/préciser" les éléments vus à la ligne du dessus (comme `r` pour le rayon (radius) d'un cercle) (https://developer.mozilla.org/fr/docs/Web/SVG/Attribute)
  - Un validateur de SVG (jusqu'à la 1.1) : http://validator.w3.org/

# Le programme Python pour parser un SVG

## Utiliser `xml.etree.ElementTree` - The ElementTree XML API

- Doc Python : https://docs.python.org/3/library/xml.etree.elementtree.html

À suivre

# Applications

À suivre



#Licence CC0
- Voir le fichier

